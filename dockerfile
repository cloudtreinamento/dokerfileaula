FROM  ubuntu 

LABEL MAINTAINER = "Antonio Henriques "
LABEL APP_VERSION = "1.0.0"

ENV NPM_VERSION=8 ENVIRONMENT=PROD

RUN apt-get update && apt-get install -y git nano npm

WORKDIR /usr/share/myapp

RUN npm build

COPY requeriments.txt requeriments.txt

ADD ./files.tar.gz ./
